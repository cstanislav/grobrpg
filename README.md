# Grob RPG

Inspired by https://dev.opera.com/articles/3d-games-with-canvas-and-raycasting-part-1/
Game mechanics inspired by Doom RPG. Assets, such as textures and sprites, are borrowed from the [Freedoom project](https://freedoom.github.io/). For more information about the license, see the COPYING file.

A grid-based game engine with a software renderer which uses raycasting to draw the scene column by column. It is recommended to reduce the window width to increase performance.

Latest version is always available [here](https://cingel.sk/prototypes/grobrpg).

## Controls

- WASD - move
- QE - strafe left/right
- RF - look up/down
- TG - fly up/down

Touchscreen controls don't support looking up/down and flying (yet).

    Grob RPG
    Copyright (C) 2021  Stanislav Cingel

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
