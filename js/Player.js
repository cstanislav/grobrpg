class Player extends Thing {
	constructor(x, y, z, rot) {
		super(x, y, z);
		
		this.rot = rot;
		this.height = 0.5; // TODO zmenit na 0 a kameru posunut vyssie
		this.angle = 0;
		this.moveX = 0; // Move forwards, backwards
		this.moveY = 0; // Strafe left, right
		this.turn = 0; // Rotate left, right
		this.moveXRemaining = 0;
		this.moveYRemaining = 0;
		this.turnRemaining = 0;
		
		this.moving = 0; // Any of above
		this.roundEnd = 0;
		
		this.turnSpeed = Math.PI * 0.05;
		this.moveSpeed = 0.10;
		
		this.rotGrid = 0; // rot = rotSimple/2 * PI
	}
	
	update() {
		this.move();
	}
	
	move() {
		if (this.moveX != 0) { // Move forward, backward
			if (this.moveXRemaining >= this.moveSpeed) {
				this.moveXRemaining -= this.moveSpeed;
				var moveStep = this.moveX * this.moveSpeed;
			} else {
				this.moveX = 0;
				var moveStep = this.moveX * this.moveXRemaining;
			}
			var newX = this.x + Math.cos(this.rot) * moveStep;
			var newY = this.y + Math.sin(this.rot) * moveStep;

			this.x = newX;
			this.y = newY;
			this.roundEnd = 1;
		}

		if (this.moveY != 0) {
			// Strafe left, right
			if (this.moveYRemaining >= this.moveSpeed) {
				this.moveYRemaining -= this.moveSpeed;
				var moveStep = this.moveY * this.moveSpeed;
			} else {
				this.moveY = 0;
				var moveStep = this.moveY * this.moveYRemaining;
			}
			var newX = this.x - Math.sin(this.rot) * moveStep;
			var newY = this.y + Math.cos(this.rot) * moveStep;

			this.x = newX;
			this.y = newY;
			this.roundEnd = 1;
		}

		if (this.turn != 0) {
			// Turn left, right
			this.rot = this.rot + 10*CIRCLE;
			if (this.turnRemaining > this.turnSpeed) {
				this.turnRemaining -= this.turnSpeed;
				this.rot += this.turn * this.turnSpeed;
			} else {
				this.rot += this.turn * this.turnRemaining;
				this.turn = 0;
			}
			this.rot %= CIRCLE;
			this.roundEnd = 1;
		}
		
		if (this.moveX == 0 && this.moveY == 0 && this.turn == 0) {
			// Koniec pohybu
			this.moving = 0;
			if (this.roundEnd == 1) {
				this.roundEndCleanup();
			}
			this.roundEnd = 0;
			this.xGrid = Math.floor(this.x);
			this.yGrid = Math.floor(this.y);
			this.x = this.xGrid + 0.5;
			this.y = this.yGrid + 0.5;
			this.rotGrid = Math.round(
				( ( this.rot + 2*Math.PI + 0.1 ) % ( 2*Math.PI ) ) / Math.PI*2
			);
			this.rot = (this.rotGrid/2) * Math.PI;
		}
	}
	
	moveForward() {
		if (!this.moving) {
			this.moveX = 1;
			this.moveXRemaining = 1;
			this.moving = 1;
		}
	}
	
	moveBackward() {
		if (!this.moving) {
			this.moveX = -1;
			this.moveXRemaining = 1;
			this.moving = 1;
		}
	}
	
	strafeLeft() {
		if (!this.moving) {
			this.moveY = 1;
			this.moveYRemaining = 1;
			this.moving = 1;
		}
	}
	
	strafeRight() {
		if (!this.moving) {
			this.moveY = -1;
			this.moveYRemaining = 1;
			this.moving = 1;
		}
	}
	
	turnLeft() {
		if (!this.moving) {
			this.turn = 1; // bolo zle, lebo y coord bol naopak...
			this.turnRemaining = Math.PI / 2;
			this.moving = 1;
		}
	}
	
	turnRight() {
		if (!this.moving) {
			this.turn = -1;
			this.turnRemaining = Math.PI / 2;
			this.moving = 1;
		}
	}
	
	roundEndCleanup() {
	}

	lookUp() {
		this.angle += 10;
	}
	lookDown() {
		this.angle -= 10;
	}
	flyUp() {
		this.height += 0.1;
	}
	flyDown() {
		this.height -= 0.1;
	}
}