class Camera extends Thing {
	constructor(fov, target = null) {
		super(0,0);
		this.fov = fov;
		this.target = target;
		this.angle = 0; // look up/down;
	}
	
	update() {
		if (this.target != null) {
			this.x = this.target.x;
			this.y = this.target.y;
			this.rot = this.target.rot;
			this.z = this.target.z + this.target.height;
			this.angle = this.target.angle;
		}
	}
	
	moveTo(x,y,rot) {
		this.x = x;
		this.y = y;
		this.rot = rot;
		this.target = null;
	}
}