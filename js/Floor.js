class Floor {
    constructor(level, textureMap, canvasWidth, canvasHeight) {
		this.level = level;
		this.fogColor = this.hexToRGB(level.fogColor);

		this.floorCanvases = [];
		this.floorCtxes = [];
		this.floorDatas = [];
		this.ceilCanvases = [];
		this.ceilCtxes = [];
		this.ceilDatas = [];

		this.pixelCanvases = [];
		this.pixelCtxes = [];
		this.imageDatas = [];

		for (var z = Z_MIN; z <= Z_MAX; z += 1) {
			// Prepare floor texture canvases
			this.floorCanvases[z] = document.createElement('canvas');
			// TODO nastavit podla rozmerov textureMap.image textury
			this.floorCanvases[z].width = this.level.sizeX*64; 
			this.floorCanvases[z].height = this.level.sizeY*64;
			this.floorCtxes[z] = this.floorCanvases[z].getContext('2d');

			this.ceilCanvases[z] = document.createElement('canvas');
			// TODO nastavit podla rozmerov textureMap.image textury
			this.ceilCanvases[z].width = this.level.sizeX*64;
			this.ceilCanvases[z].height = this.level.sizeY*64;
			this.ceilCtxes[z] = this.ceilCanvases[z].getContext('2d');
			
			for (var x = 0; x < this.level.sizeX; x++) {
				for (var y = 0; y < this.level.sizeY; y++) {

					var blockFloor = this.level.getBlock(x, y, z-1);
					if (blockFloor != undefined) {
						var textureFloor = textureMap.getTexture(blockFloor.getTop());
						if (textureFloor != undefined) {
							this.floorCtxes[z].imageSmoothingEnabled = false;
							this.floorCtxes[z].drawImage(
								textureMap.image,
								textureFloor.startX, textureFloor.startY,
								textureFloor.width, textureFloor.height,
								64*x, 64*y, 64, 64
							);
						}
					}

					var blockCeil = this.level.getBlock(x, y, z+1);
					if (blockCeil != undefined) {
						var textureCeil = textureMap.getTexture(blockCeil.getBottom());
						if (textureCeil != undefined) {
							this.ceilCtxes[z].imageSmoothingEnabled = false;
							this.ceilCtxes[z].drawImage(
								textureMap.image,
								textureCeil.startX, textureCeil.startY,
								textureCeil.width, textureCeil.height,
								64*x, 64*y, 64, 64
							);
						}
					}
				}
			}

			
			this.floorCtxes[z].fillStyle = level.fogColor;
			this.floorCtxes[z].globalAlpha = 1-level.maxLight;
			this.floorCtxes[z].globalCompositeOperation = "source-atop";
			this.floorCtxes[z].fillRect(
				0, 0, this.floorCanvases[z].width,
				this.floorCanvases[z].height
			);
			this.floorDatas[z] = this.floorCtxes[z].getImageData(
				0, 0, this.floorCanvases[z].width,
				this.floorCanvases[z].height
			).data;

			this.ceilCtxes[z].fillStyle = level.fogColor;
			this.ceilCtxes[z].globalAlpha = 1-level.maxLight;
			this.ceilCtxes[z].globalCompositeOperation = "source-atop";
			this.ceilCtxes[z].fillRect(
				0, 0, this.ceilCanvases[z].width, this.ceilCanvases[z].height
			);
			this.ceilDatas[z] = this.ceilCtxes[z].getImageData(
				0, 0, this.ceilCanvases[z].width, this.ceilCanvases[z].height
			).data;

			// Prepare floor pixel canvases
			this.pixelCanvases[z] = document.createElement('canvas');
			this.pixelCanvases[z].width = canvasWidth;
			this.pixelCanvases[z].height = canvasHeight;
			this.pixelCtxes[z] = this.pixelCanvases[z].getContext('2d');

			this.resetImageData(z);
		}
	}

	hexToRGB(hex) {
		var r = parseInt(hex.slice(1, 3), 16),
			g = parseInt(hex.slice(3, 5), 16),
			b = parseInt(hex.slice(5, 7), 16);

		return [r, g, b];
	}

	getIndexByCoords(x, y, width) {
		return (y*width+x)*4;
	}

	resetImageData(z) {
		this.pixelCtxes[z].clearRect(
			0, 0,
			this.pixelCanvases[z].width, this.pixelCanvases[z].height
		);
		this.imageDatas[z] = this.pixelCtxes[z].getImageData(
			0, 0,
			this.pixelCanvases[z].width, this.pixelCanvases[z].height
		);
	}

	renderZ(ctx, z) {
		if (z > Z_MAX) {
			return;
		}

		this.pixelCtxes[z].putImageData(this.imageDatas[z], 0, 0);
		ctx.drawImage(
			this.pixelCanvases[z],
			0, 0, this.pixelCanvases[z].width, this.pixelCanvases[z].height,
			0, 0, ctx.canvas.width, ctx.canvas.height
		);
		this.resetImageData(z);
	}

	debug(ctx) {
		var z = 1;
		ctx.drawImage(
			this.ceilCanvases[z],
			0, 0, this.ceilCanvases[z].width, this.ceilCanvases[z].height,
			0, 0, ctx.canvas.width/4, ctx.canvas.height/4
		);
	}

	render(ctx) {
		for (var z = Z_MIN; z <= Z_MAX; z += 1) {
			this.renderZ(ctx, z);
		}
	}

	adjustSize(width, height) {
		for (var z = Z_MIN; z <= Z_MAX; z += 1) {
			// TODO maybe put an if here?
			this.pixelCanvases[z].width = width;
			this.pixelCanvases[z].height = height;
		}
	}

    getFloorColor(x, y, z) {
		if (
			x < 0 || y < 0 ||
			x >= this.floorCanvases[z].width ||
			y >= this.floorCanvases[z].height
		) {
			return false;
		}

		var redIndex = this.getIndexByCoords(
			x, y, this.floorCanvases[z].width
		);

		if (this.floorDatas[z][redIndex+3] == 0) { // alpha 0 = outside the canvas
			return false;
		}
		return [
			this.floorDatas[z][redIndex],
			this.floorDatas[z][redIndex+1],
			this.floorDatas[z][redIndex+2]
		];
	}

	getCeilColor(x, y, z) {
		if (
			x < 0 || y < 0 ||
			x >= this.ceilCanvases[z].width ||
			y >= this.ceilCanvases[z].height
		) {
			return false;
		}

		var redIndex = this.getIndexByCoords(
			x, y, this.ceilCanvases[z].width
		);

		if (this.ceilDatas[z][redIndex+3] == 0) { // alpha 0 = outside the canvas
			return false;
		}

		return [
			this.ceilDatas[z][redIndex],
			this.ceilDatas[z][redIndex+1],
			this.ceilDatas[z][redIndex+2]
		];
	}

	drawFloorPixel(x, y, z, color, fogLevel, isCeiling) {
		function addColors(color1, color2, alpha) {
			return color1*(1-alpha) + color2*alpha;
		}

		y = this.pixelCtxes[z].canvas.height - y;

		if (
			x < 0 || y < 0 ||
			x > this.pixelCanvases[z].width ||
			y > this.pixelCanvases[z].height
		) {
			return;
		}

		if (isCeiling) {
			x = this.pixelCanvases[z].width-x;
		}

		var redIndex = this.getIndexByCoords(x, y, this.pixelCanvases[z].width);

		if (fogLevel) {
			this.imageDatas[z].data[redIndex] = addColors(color[0], this.fogColor[0], fogLevel);
			this.imageDatas[z].data[redIndex+1] = addColors(color[1], this.fogColor[1], fogLevel);
			this.imageDatas[z].data[redIndex+2] = addColors(color[2], this.fogColor[2], fogLevel);
			this.imageDatas[z].data[redIndex+3] = 255;
		} else {
			this.imageDatas[z].data[redIndex] = color[0];
			this.imageDatas[z].data[redIndex+1] = color[1];
			this.imageDatas[z].data[redIndex+2] = color[2];
			this.imageDatas[z].data[redIndex+3] = 255;
		}
	}
}