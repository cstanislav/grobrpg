class Debug {
    constructor(object, game) {
        this.object = $(object);
        this.game = game;
        this.lines = [];
    }

    push(line) {
        this.lines.push(line);
    }

    update() {
        this.object.html("");
        this.render();
        this.lines = [];
    }

    render() {
        this.lines.forEach(line => {
            this.object.append(line + "\n");
        });
    }
}