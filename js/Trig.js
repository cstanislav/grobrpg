class Trig {
    constructor() {
        this.sinTable = [];
        this.cosTable = [];
        this.tanTable = [];
        this.asinTable = [];
        this.sqrtTable = [];
        this.sqrTable = [];
        this.tanTable = [];
        this.atanTable = [];
    }

    update() {
        // this.game.gui.debug.push(
        //     this.sinTable.length +
        //     this.cosTable.length +
        //     this.tanTable.length +
        //     this.asinTable.length +
        //     this.sqrtTable.length +
        //     this.sqrTable.length +
        //     this.tanTable.length +
        //     this.atanTable.length
        // );
    }

    sin(rayAngle) {
        if (this.sinTable[rayAngle] != undefined) {
            return this.sinTable[rayAngle];
        } else {
            var sin = Math.sin(rayAngle);
            this.sinTable[rayAngle] = sin;
            return sin;
        }
    }

    cos(rayAngle) {
        if (this.cosTable[rayAngle] != undefined) {
            return this.cosTable[rayAngle];
        } else {
            var cos = Math.cos(rayAngle);
            this.cosTable[rayAngle] = cos;
            return cos;
        }
    }

    asin(x) {
        return Math.asin(x);
        // if (this.asinTable[x] != undefined) {
        //     return this.asinTable[x];
        // } else {
        //     var val = Math.asin(x);
        //     this.asinTable[x] = val;
        //     return val;
        // }
    }

    sqrt(x) {
        return Math.sqrt(x);
        // if (this.sqrtTable[x] != undefined) {
        //     return this.sqrtTable[x];
        // } else {
        //     var val = Math.sqrt(x);
        //     this.sqrtTable[x] = val;
        //     return val;
        // }
    }

    sqr(x) {
        return Math.pow(x, 2);
        // if (this.sqrTable[x] != undefined) {
        //     return this.sqrTable[x];
        // } else {
        //     var val = Math.pow(x, 2);
        //     this.sqrTable[x] = val;
        //     return val;
        // }
    }

    tan(x) {
        // return Math.tan(x);
        if (this.tanTable[x] != undefined) {
            return this.tanTable[x];
        } else {
            var val = Math.tan(x);
            this.tanTable[x] = val;
            return val;
        }
    }

    atan(x) {
        return Math.atan(x);
        // if (this.atanTable[x] != undefined) {
        //     return this.atanTable[x];
        // } else {
        //     var val = Math.atan(x);
        //     this.atanTable[x] = val;
        //     return val;
        // }
    }
}