// Inspiration taken from https://dev.opera.com/articles/3d-games-with-canvas-and-raycasting-part-1/
// Game mechanics inspired by Doom RPG

var CIRCLE = Math.PI * 2;

var SCREEN_FOV_VERT = 1.65; // Vertical (rad) inak 1.8
var STRIP_WIDTH = 2; 
var CANVAS_UPSCALE = 2;
var FLOOR_WIDTH = 1;
var FLOOR_HEIGHT = 1;
var Z_MIN = -2;
var Z_MAX = 6;
var TOTAL_UPSCALE = STRIP_WIDTH*CANVAS_UPSCALE;

// TODO STRIP_WIDTH = 1 - pokazena podlaha

var loop;
function init(textureMap) {
    loop = new GameLoop(textureMap);
	
    loop.start(function frame(seconds) {
        loop.game.update();
    });
}

function loaded() {
    init(textureMap);
}

function saveMap() {
    console.log(loop.game);
}

textureMap = new TextureMap("img/walls.png", loaded);
