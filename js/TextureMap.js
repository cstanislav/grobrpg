class TextureMap {
	constructor(src, callback) {
		this.image = new Image();
		this.image.src = src;
		
		this.textures = [];
		this.initTextures();

		this.image.onload = callback;
	}
	
	addTexture(texture) {
		this.textures[texture.id] = texture;
	}

	getTexture(id) {
		return this.textures[id];
	}
	
	initTextures() {
		this.addTexture(new Texture("BRICK", 0, 0, 64, 64, false));
		this.addTexture(new Texture("CRATE", 64, 0, 64, 64, false));
		this.addTexture(new Texture("GRASS", 192, 0, 64, 64, false));
		this.addTexture(new Texture("COBBLE", 256, 0, 64, 64, true));
		this.addTexture(new Texture("PLAYER", 0, 64, 64, 64, false));
		this.addTexture(new Texture("GRAYGUY", 64, 64, 64, 64, false));
		this.addTexture(new Texture("SKYBOX", 0, 256, 256, 256, false));
	}
}