class Texture {
	constructor(id, startX, startY, width, height, translucent) {
		this.id = id;
		this.startX = startX;
		this.startY = startY;
		this.width = width;
		this.height = height;
		this.translucent = translucent;
	}
}