class Raycaster {
    constructor(level) {
        this.level = level;
        this.rays = [];
        this.trig = this.level.game.trig;
        this.cardinals = new Cardinal();
    }

    clear() {
        this.rays = [];
    }

    castRay(pointX, pointY, rayAngle, range) {
        function ray(thisStep) {
            var stepX = step(sin, cos, thisStep.x, thisStep.y);
            var stepY = step(cos, sin, thisStep.y, thisStep.x, true);
            var nextStep = stepX.length2 < stepY.length2
                ? inspect(stepX, 1, 0, thisStep.distance, stepX.y)
                : inspect(stepY, 0, 1, thisStep.distance, stepY.x);

            if (
                nextStep.distance > range ||
                nextStep.x < 0 ||
                nextStep.y < 0 ||
                nextStep.x > self.level.sizeX ||
                nextStep.y > self.level.sizeY
            ) {
                // kedy sa luc zastavi - ALL GOOD
                return [thisStep];
            } else if (nextStep.walls.length == 0) {
                return ray(nextStep);
            } else {
                return [nextStep].concat(ray(nextStep));
            }

            return rayTemp;
        }

        function step(rise, run, x, y, inverted) {
            if (run === 0) return noWall;
            var dx = run > 0 ? Math.floor(x + 1) - x : Math.ceil(x - 1) - x;
            var dy = dx * (rise / run);
            var rayTemp = {
                x: inverted ? y + dy : x + dx,
                y: inverted ? x + dx : y + dy,
                length2: dx * dx + dy * dy
            };

            return rayTemp;
        }

        function inspect(step, shiftX, shiftY, distance, offset) {
            var dx = cos < 0 ? 0 : 1;
            var dy = sin < 0 ? 0 : 1;
            var vert = shiftX; // ak je shiftX 1, je vertikalna, ak je shiftY 1, je horizontalna. nikdy nie su obidve naraz 1.
            if (vert) {
                var side = 1 - dx; // najprv vrchna a potom spodna stena v indexoch
            } else {
                var side = 1 - dy; // najprv lava a potom prava stena v indexoch
            }

            var x = Math.floor(step.x);
            var y = Math.floor(step.y);

            step.shiftX = shiftX;
            step.shiftY = shiftY;
            step.side = side;

            step.distance = distance + self.trig.sqrt(step.length2);
            /*if (shiftX) step.shading = cos < 0 ? 2 : 0; // jednu stranu robia tmavsiu ako druhu
            else step.shading = sin < 0 ? 2 : 1;*/        // jednu stranu robia tmavsiu ako druhu
            step.offset = offset - Math.floor(offset);
            
            var cardinal = new Cardinal(vert, side);
            if (
                cardinal.direction == cardinal.directions.south ||
                cardinal.direction == cardinal.directions.east
            ) { // ak sa pozerame zo severu alebo vychodu, otoc texturu
                step.offset = 1 - step.offset;
            }

            if (cardinal.direction == cardinal.directions.east) {
                x--;
            }

            if (cardinal.direction == cardinal.directions.north) {
                y--;
            }

            // if (cardinal.direction != 0) {
            //     debugger;
            // }

            // if vert = 0: 0 - bottom, 1 - top
            // if vert = 1: 0 - left, 1 - right
            

            step.walls = [];
            for (var z = 0; z < self.level.sizeZ; z++) {
                var block = self.level.getBlock(x, y, z);
                if (block != undefined) {
                    step.walls.push( [z, block.getWall(cardinal)] );
                }
            }

            return step;
        }

        var self = this;

        var sin = this.trig.sin(rayAngle);
        var cos = this.trig.cos(rayAngle);

        var noWall = { length2: Infinity };
        var rayTemp = ray({
            x: pointX,
            y: pointY,
            distance: 0
        }, 0);

        this.rays.push(rayTemp);

        return rayTemp;
    }
}