class GUI {
	constructor(game, loadedFiles, debug) {
		this.game = game;
		this.screen = new Screen(
			"#screen",
			loadedFiles,
			game.level
		);
		this.minimap = new Minimap("#minimap", game.level);
		this.debug = debug;
	}

	update() {
		this.screen.update();
		this.minimap.update();
		this.debug.update();
	}
}