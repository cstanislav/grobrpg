class Game {
	constructor(loadedFiles) {
		this.trig = new Trig();
		this.level = new Level(this);
		this.debug = new Debug("#debug", this);
		this.gui = new GUI(this, loadedFiles, this.debug);
		this.player = new Player(
			this.level.playerX,
			this.level.playerY,
			this.level.playerZ,
			this.level.playerRot
		);
		this.level.addThing(this.player);
		var camera = new Camera(100,this.player);
		this.level.camera = camera;
		this.level.addThing(camera);
		
		this.input = new InputHandler(this);
	}
	
	update() {
		this.input.update();
		this.level.update();
		this.gui.update();
		this.trig.update();
	}	
}