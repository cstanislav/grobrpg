class ZBuffer {
    constructor(textureMap, level, floor, debug) {
        this.buffer = [];
        this.floor = floor;
        this.debug = debug;
        this.textureMap = textureMap;
        this.fogColor = level.fogColor;
        this.maxLight = level.maxLight;

        // prepare max light canvas
        this.maxLightCanvas = document.createElement('canvas');
        this.maxLightCanvas.width = 512; // TODO nastavit podla rozmerov textureMap.image
        this.maxLightCanvas.height = 512;
        this.maxLightCtx = this.maxLightCanvas.getContext('2d');

        this.maxLightCtx.imageSmoothingEnabled = false; // fixes upscaling blur
        this.maxLightCtx.drawImage(this.textureMap.image, 0, 0, 512, 512, 0, 0, 512, 512);

        this.maxLightCtx.fillStyle = this.fogColor;
        this.maxLightCtx.globalAlpha = 1-this.maxLight;
        this.maxLightCtx.globalCompositeOperation = "source-atop";
        this.maxLightCtx.fillRect(0, 0, 512, 512);
        this.maxLightCtx.globalCompositeOperation = "source-over";
        this.maxLightCtx.globalAlpha = 1;

        // prepare relief canvas
        this.reliefCanvas = document.createElement('canvas');
        this.reliefCanvas.width = 512; // TODO nastavit podla rozmerov textureMap.image
        this.reliefCanvas.height = 512;
        this.reliefCtx = this.reliefCanvas.getContext('2d');

        this.reliefCtx.imageSmoothingEnabled = false; // fixes upscaling blur
        this.reliefCtx.drawImage(this.textureMap.image, 0, 0, 512, 512, 0, 0, 512, 512);

        this.reliefCtx.fillStyle = this.fogColor;
        this.reliefCtx.globalCompositeOperation = "source-atop";
        this.reliefCtx.fillRect(0, 0, 512, 512);
        this.reliefCtx.globalCompositeOperation = "source-over";
    }

    push(distance, patch) {
        // console.log(distance, patch);
        // debugger;
        if (this.buffer.length == 0) {
            this.buffer = [patch];
        } else {
            var j;
            for (j = 0; j < this.buffer.length; j++) {
                if (distance >= this.buffer[j].bufferDist) {
                    break;
                }
            }
            this.buffer.splice(j, 0, patch);
        }
    }

    debugRenderRelief(context) {
        context.drawImage(this.reliefCanvas, 0, 0, 512, 512, 0, 0, 512, 512);
    }

    render(context) {
        context.imageSmoothingEnabled = false;

        let segmentsLength = this.buffer.length - 1;
		for (var numSegment = 0; numSegment < this.buffer.length; numSegment++) {
            if (this.buffer[numSegment].isFloor) {
                this.floor.renderZ(context, this.buffer[numSegment].floorZ);
            }
            let isTranslucent = this.buffer[numSegment].isTranslucent;
            var fogLevel = this.buffer[numSegment].fogLevel;

            if (fogLevel != 1) {
                // copy original (max light) texture from max light canvas
                context.imageSmoothingEnabled = false; // fixes upscaling blur
                context.drawImage(
                    this.maxLightCanvas,
                    this.buffer[numSegment].textureStartX,
                    this.buffer[numSegment].textureStartY,
                    this.buffer[numSegment].textureSizeX,
                    this.buffer[numSegment].textureSizeY,
                    Math.floor(this.buffer[numSegment].renderStartX),
                    Math.floor(this.buffer[numSegment].renderStartY),
                    Math.ceil(this.buffer[numSegment].renderSizeX),
                    Math.ceil(this.buffer[numSegment].renderSizeY)
                );
            }

            if (fogLevel > 0) {
                // TODO ak je maxLightLevel mensi nez 1,
                // netreba odcitat maxLight, alebo (1-maxLight)?
                context.globalAlpha = fogLevel;
                if (isTranslucent) {
                    // set alpha and copy fogged texture from relief canvas
                    context.drawImage(
                        this.reliefCanvas,
                        this.buffer[numSegment].textureStartX,
                        this.buffer[numSegment].textureStartY,
                        this.buffer[numSegment].textureSizeX,
                        this.buffer[numSegment].textureSizeY,
                        Math.floor(this.buffer[numSegment].renderStartX),
                        Math.floor(this.buffer[numSegment].renderStartY),
                        Math.ceil(this.buffer[numSegment].renderSizeX),
                        Math.ceil(this.buffer[numSegment].renderSizeY)
                    );
                } else {
                    // Set alpha and draw a filled rectangle
                    context.fillStyle = this.fogColor;
                    context.fillRect(
                        Math.floor(this.buffer[numSegment].renderStartX),
                        Math.floor(this.buffer[numSegment].renderStartY),
                        Math.ceil(this.buffer[numSegment].renderSizeX),
                        Math.ceil(this.buffer[numSegment].renderSizeY)
                    );
                }
                context.globalAlpha = 1;
            }
        }
        // this.debugRenderRelief(context);
        this.clear();
    }

    clear() {
        this.buffer = [];
    }
}