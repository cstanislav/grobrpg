class Screen {
	constructor(object, textureMap, level) {
		this.object = $(object)[0];
		this.ctx = this.object.getContext("2d");

		this.level = level;

		this.numRays = Math.ceil(this.width / STRIP_WIDTH);
		this.textureMap = textureMap;
		this.floor = new Floor(level, textureMap, window.innerWidth / (TOTAL_UPSCALE * FLOOR_WIDTH), window.innerHeight / (TOTAL_UPSCALE * FLOOR_HEIGHT));
		this.zBuffer = new ZBuffer(this.textureMap, this.level, this.floor, this.level.game.debug);
		this.trig = this.level.game.trig;

		this.range = this.level.renderDistance;
	}

	render() {
		this.clearScreen(this.ctx);
		
		this.drawColumns(this.zBuffer);
		this.drawBackground(this.ctx);
		this.drawSprites(this.zBuffer);
		this.drawFloor(this.zBuffer);
		// this.floor.render(this.ctx);
		this.zBuffer.render(this.ctx);

		// this.floor.debug(this.ctx);
	}

	update() {
		this.adjustSize();
		this.render();
		this.level.game.debug.push(this.level.camera.x + " " + this.level.camera.y + " " + this.level.camera.z);
	}

	adjustSize() {
		if (
			this.windowWidth == window.innerWidth &&
			this.windowHeight == window.innerHeight &&
			this.windowWidth != undefined &&
			this.windowHeight != undefined
		) {
			return;
		}

		this.windowWidth = window.innerWidth;
		this.windowHeight = window.innerHeight;

		this.ctx.canvas.width = window.innerWidth / CANVAS_UPSCALE;
		this.ctx.canvas.height = window.innerHeight / CANVAS_UPSCALE;
		this.width = this.ctx.canvas.width;
		this.height = this.ctx.canvas.height;
		//this.ctx.imageSmoothingEnabled = true;

		this.floor.adjustSize(
			window.innerWidth / (TOTAL_UPSCALE * FLOOR_WIDTH),
			window.innerHeight / (TOTAL_UPSCALE * FLOOR_HEIGHT)
		);

		let temp = this.trig.tan(SCREEN_FOV_VERT / 2) * this.width / this.height;
		this.fov = 2 * this.trig.atan(temp);

		if (this.fov < SCREEN_FOV_VERT) {
			this.fov = SCREEN_FOV_VERT;
		}

		this.numRays = Math.ceil(this.width / STRIP_WIDTH);
		this.viewDist = (this.width / 2) / this.trig.tan(this.fov / 2);
	}

	angleBetween(n, a, b) {
		n = (CIRCLE + (n % 360)) % CIRCLE;
		a = (CIRCLE * 10 + a) % CIRCLE;
		b = (CIRCLE * 10 + b) % CIRCLE;

		if (a < b) {
			return !((a <= n) && (n <= b));
		}
		return !((a <= n) || (n <= b));
	}

	projectRev(rayAngle, bottom, camHeight) {
		var top = bottom;
		var wallSegmentHeight = (top - this.level.camera.angle) / camHeight;
		var z = this.viewDist / wallSegmentHeight;
		return z / this.trig.cos(rayAngle);
	}

	getBufferDist(rayAngle, distance) {
		return Math.round(this.viewDist / (distance * this.trig.cos(rayAngle)));
	}

	project(rayAngle, distance, spriteRadius = 0) {
		// preventing "fisheye effect"

		var z = distance * this.trig.cos(rayAngle);

		if (spriteRadius != 0) {
			z = Math.max(z-spriteRadius, 0);
		}

		var bufferDist = z;
		var wallHeight = Math.round(this.viewDist / z * this.level.sizeZ);
		var wallSegmentHeight = wallHeight / this.level.sizeZ;

		var bottom = Math.round(this.height / 2) +
			this.level.camera.z * wallSegmentHeight
			+ this.level.camera.angle;
		var top = bottom - wallHeight;

		return {
			top: top,
			bottom: Math.round(bottom),
			height: wallHeight,
			segmentHeight: wallSegmentHeight,
			z: distance,
			bufferDist: bufferDist
		};
	}

	getFogLevel(dist) {
		if (dist < this.level.fogStart) {
			var lightLevel = 1;
		} else if (dist > this.level.lightRange) {
			var lightLevel = 0;
		} else {
			var lightRatio = 1 / (this.level.lightRange - this.level.fogStart);
			var lightLevel = -lightRatio * (dist - this.level.lightRange);
		}
		return 1-lightLevel;
	}

	clearScreen(ctx) {
		ctx.clearRect(0, 0, this.width, this.height);
	}


	// DRAWING BACKGROUND

	drawBackground(ctx) {
		// Ceiling color / skybox
		ctx.fillStyle = this.level.ceilColor;
		ctx.fillRect(0, 0, this.width, this.height);

		if (this.level.skybox) {
			var texture = this.textureMap.getTexture("SKYBOX");

			var sizeRatio = this.width / texture.width;
			var textureRatio = texture.width / texture.height;
			var skyRatio = this.width / (this.height / 2);

			if (textureRatio > skyRatio) { // portrait
				var skyHeight = this.height / 2;
				var skyWidth = skyHeight * textureRatio;

				var left = this.level.camera.rot % (Math.PI) / Math.PI;
				left *= skyWidth;
			} else { // landscape
				var skyWidth = texture.width * sizeRatio;
				var skyHeight = texture.height * sizeRatio;

				var left = this.level.camera.rot % (Math.PI) / Math.PI;
				left *= skyWidth;
			}

			if (left != 0) {
				this.ctx.imageSmoothingEnabled = false; // fixes upscaling blur
				this.ctx.drawImage(
					this.textureMap.image,

					texture.startX,
					texture.startY,
					texture.width,
					texture.height,

					left - this.width,
					(this.height / 2) - skyHeight + this.level.camera.angle,
					skyWidth,
					skyHeight
				);
			}

			this.ctx.imageSmoothingEnabled = false; // fixes upscaling blur
			this.ctx.drawImage(
				this.textureMap.image,

				texture.startX,
				texture.startY,
				texture.width,
				texture.height,

				left,
				(this.height / 2) - skyHeight + this.level.camera.angle,
				skyWidth,
				skyHeight
			);
		}
		
		// Floor color - gradient
		var grd = this.ctx.createLinearGradient(0, this.height / 2  + this.level.camera.angle, 0, this.height + this.level.camera.angle);
		grd.addColorStop(0, this.level.floorColor2);
		grd.addColorStop(1, this.level.floorColor);

		this.ctx.fillStyle = grd;
		this.ctx.fillRect(0, Math.max(this.height / 2 + this.level.camera.angle, 0), this.width, this.height);
	}


	// DRAWING WALLS

	drawColumns(buffer) {
		this.level.raycaster.clear();
		for (var numRay = 0; numRay < this.numRays; numRay++) {
			var rayScreenPos = (-this.numRays / 2 + numRay) * STRIP_WIDTH;
			var rayViewDist =
				this.trig.sqrt(
					this.trig.sqr(rayScreenPos) +
					this.trig.sqr(this.viewDist)
				);
			var rayAngle = this.trig.asin(rayScreenPos / rayViewDist);

			var ray = this.level.raycaster.castRay(
				this.level.camera.x,
				this.level.camera.y,
				this.level.camera.rot + rayAngle,
				this.range
			);

			this.drawColumn(buffer, this.numRays - numRay - 1, ray, rayAngle);
		}
	}

	splitSegments(segments, min, max) {
		var j;
		var newSegments = [];
		min--;
		max++;
		for (var i = 0; i < segments.length; i++) {
			if (max < segments[i][0]) {
				newSegments.push(segments[i]);
				continue;
			}
			if (min > segments[i][1]) {
				newSegments.push(segments[i]);
				continue;
			}
			// aspon jedna hranica je v segmente

			if (min >= segments[i][0] && max <= segments[i][1]) {
				// obe hranice su vnutri
				newSegments.push([segments[i][0], min]);
				newSegments.push([max, segments[i][1]]);
				continue;
			}
			// len jedna hranica je vnutri

			if (min > segments[i][0]) {
				newSegments.push([min, segments[i][1]]);
				continue;
			}

			if (max < segments[i][1]) {
				newSegments.push([segments[i][0], max]);
				continue;
			}
		}
		return newSegments;
	}

	drawColumn(buffer, numRay, ray, angle) {
		var prevLowHeight = 0;

		// var segments = [[0, Math.ceil(this.height/STRIP_WIDTH)]];

		for (var s = 0; s < ray.length; s++) {
			var step = ray[s];
			if (step.walls == undefined || step.walls.length == []) continue;

			var wall = this.project(angle, step.distance);

			for (var i = 0; i < step.walls.length; i++) {
				if (step.walls[i][1] == null) {
					continue;
				}

				var textureId = step.walls[i][1].textureId;
				var texture = this.textureMap.getTexture(textureId);
				var textureX = Math.floor(texture.width * (1 - step.offset));
				var wallTop = wall.top + wall.segmentHeight * (this.level.sizeZ - step.walls[i][0] - 1);

				var fogLevel = this.getFogLevel(wall.z);

				// if (!texture.translucent) {
				// 	segments = this.splitSegments(segments, wallTop, wallTop+wall.segmentHeight);
				// }

				var bufferDist = wall.bufferDist + 1000*Math.abs(Math.floor(this.level.camera.z)-step.walls[i][0]);

				buffer.push(
					bufferDist,
					{
						bufferDist: bufferDist,
						textureStartX: textureX + texture.startX,
						textureStartY: texture.startY,
						textureSizeX: 1,
						textureSizeY: texture.height,
						renderStartX: numRay * STRIP_WIDTH,
						renderStartY: wallTop,
						renderSizeX: STRIP_WIDTH,
						renderSizeY: wall.segmentHeight,
						fogLevel: fogLevel,
						fogColor: this.level.fogColor,
						isTranslucent: texture.translucent,
						isFloor: false,
						isCeiling: false,
					}
				);
			}
			prevLowHeight = step.lowHeight;
		}
		
		this.drawColumnFloor(numRay, angle);
	}


	//// DRAWING FLOOR

	drawColumnFloor(numRay, angle) {
		if (numRay % FLOOR_WIDTH != 0) {
			return;
		}

		var numRows = Math.ceil(this.ctx.canvas.height/STRIP_WIDTH);
		var floorRows = Math.max(
			Math.ceil((this.ctx.canvas.height/2-this.level.camera.angle)/STRIP_WIDTH),
			0
		);
		var ceilRows = numRows - floorRows;

		var horizontalAngle = this.level.camera.rot + angle;
		var deltaX = this.trig.cos(horizontalAngle);
		var deltaY = this.trig.sin(horizontalAngle);

		var horizontalAngleCeil = this.level.camera.rot - angle;
		var deltaXceil = this.trig.cos(horizontalAngleCeil);
		var deltaYceil = this.trig.sin(horizontalAngleCeil);

		for (var i = 0; i < numRows; i += 1) {
			var floorColor = false;
			var dist;
			var outOfRange = false;
			var z;

			if (i < floorRows) {
				for (z = Math.min(Math.floor((this.level.camera.z-0.1)), Z_MAX); z >= Z_MIN; z -= 1) {
					outOfRange = false;

					dist = this.projectRev(
						angle,
						(numRows)-(i*STRIP_WIDTH*FLOOR_HEIGHT),
						this.level.camera.z-z
					);

					if (dist > this.level.lightRange) {
						outOfRange = true;
						break;
					}

					floorColor = this.floor.getFloorColor(
						Math.floor(64 * (this.level.camera.x + deltaX*dist)),
						Math.floor(64 * (this.level.camera.y + deltaY*dist)),
						z
					);

					if (floorColor) {
						break;
					}
				}

				if (outOfRange || !floorColor) {
					continue;
				}

				var fogLevel = this.getFogLevel(dist);

				this.floor.drawFloorPixel(
					Math.floor(numRay / FLOOR_WIDTH),
					i,
					z,
					floorColor,
					fogLevel
				);
			} else {
				for (z = Math.max(Math.ceil((this.level.camera.z-0.1)), Z_MIN); z <= Z_MAX; z += 1) {
					outOfRange = false;

					dist = this.projectRev(
						angle,
						(numRows)-(i*STRIP_WIDTH*FLOOR_HEIGHT),
						z-this.level.camera.z
					);

					if (dist > this.level.lightRange) {
						outOfRange = true;
						break;
					}

					floorColor = this.floor.getCeilColor(
						Math.floor(64 * (this.level.camera.x - deltaXceil*dist)),
						Math.floor(64 * (this.level.camera.y - deltaYceil*dist)),
						z-1
					);

					if (floorColor) {
						break;
					}
				}

				if (outOfRange || !floorColor) {
					continue;
				}

				var fogLevel = this.getFogLevel(Math.abs(dist));

				this.floor.drawFloorPixel(
					numRay / FLOOR_WIDTH + 1,
					i,
					z,
					floorColor,
					fogLevel,
					true
				);
			}
		}
	}

	drawSprites(buffer) {
		let thingsLength = this.level.things.length;

		for (var numThing = 0; numThing < thingsLength; numThing++) {
			if (!this.level.things[numThing].textureId)
				continue;

			var vectorX = this.level.things[numThing].x - this.level.camera.x;
			var vectorY = this.level.things[numThing].y - this.level.camera.y;

			var thingAngle = Math.atan2(vectorY, vectorX);
			var visible = this.angleBetween(
				thingAngle,
				this.level.camera.rot + (this.fov / 2) + 0.2,
				this.level.camera.rot - (this.fov / 2) - 0.2
			);
			
			if (visible) {
				var spriteDist = this.trig.sqrt(
					this.trig.sqr(vectorX) + this.trig.sqr(vectorY)
				);

				var spriteAngle = thingAngle - this.level.camera.rot;
				var spriteX = this.trig.tan(spriteAngle) * this.viewDist;
				spriteX /= -STRIP_WIDTH;
				spriteX += this.numRays/2;

				this.drawSprite(
					buffer,
					spriteX,
					spriteDist,
					this.level.things[numThing].z,
					this.level.things[numThing].textureId,
					this.level.things[numThing].radius,
					spriteAngle
				);
			}
		}
	}

	drawSprite(buffer, stripNum, distance, z, textureId, radius, rayAngle) {
		var sprite = this.project(rayAngle, distance, radius);
		var segHeight = sprite.segmentHeight;
		var left = Math.floor(stripNum * STRIP_WIDTH) - segHeight / 2;
		var top = sprite.top + segHeight*(this.level.sizeZ - z - 1);
		var texture = this.textureMap.getTexture(textureId);

		var fogLevel = this.getFogLevel(sprite.z);

		var bufferDist = sprite.bufferDist + 1000*Math.abs(Math.floor(this.level.camera.z)-z);

		buffer.push(
			bufferDist,
			{
				bufferDist: bufferDist,
				textureStartX: texture.startX,
				textureStartY: texture.startY,
				textureSizeX: texture.width,
				textureSizeY: texture.height,
				renderStartX: left,
				renderStartY: top,
				renderSizeX: segHeight,
				renderSizeY: segHeight,
				fogLevel: fogLevel,
				fogColor: this.level.fogColor,
				isTranslucent: true,
				isFloor: false,
				isCeiling: false,
			}
		);
	}

	drawFloor(buffer) {
		for (var z = Z_MIN; z <= Z_MAX; z += 1) {
			var bufferDist = 1000 * Math.ceil(Math.abs(this.level.camera.z - z));
			buffer.push(
				bufferDist,
				{
					bufferDist: bufferDist,
					isFloor: true,
					floorZ: z
				}
			);
		}
	}
}