class GameLoop {
	constructor(loadedFiles) {
		this.game = new Game(loadedFiles);
		this.frame = this.frame.bind(this);
		this.lastTime = 0;
	}
	
	start(callback) {
		this.callback = callback;
		requestAnimationFrame(this.frame);
	}
	
	frame(time) {
		var seconds = (time - this.lastTime) / 1000;
        this.lastTime = time;
        if (seconds < 0.2) this.callback(seconds);
		var fps = 1/seconds;
		this.game.gui.debug.push(Math.floor(fps));
        requestAnimationFrame(this.frame);
	}
}